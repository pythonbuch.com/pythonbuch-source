# Source-Code of https://pythonbuch.com 

## Getting started / building 

* [Install Sphinx](https://www.sphinx-doc.org/en/master/usage/installation.html)
* Run
  ```bash
  $ git clone https://codeberg.org/pythonbuch.com/pythonbuch-source.git
  $ cd pythonbuch-source
  $ make html
  ```
