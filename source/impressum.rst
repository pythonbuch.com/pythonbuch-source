.. _impressum:

******************
Über dieses Skript
******************

Lizenz
=======

.. image:: images/cc_by_sa.png

Dieses Skript darf unter den Lizenzbestimmungen der
`Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`_
weiter verwendet werden. Dies bedeutet insbesondere:

* Es darf weiter verwendet und bearbeitet werden. Es dürfen eigene Versionen des
  Skripts erstellt werden oder etwas neues darauf aufgebaut werden.

* Es darf beliebig vervielfältigt und auf unterschiedlichen Medien weitergegeben
  werden.

* Die bearbeitete Version muss unter gleichen Lizenzbedingungen zur Verfügung
  gestellt werden.


Autoren
=======

Dieses Skript wurde bisher hauptsächlich von den folgenden Autoren verfasst:

* `Marco Schmid <https://github.com/chunyboy>`_
* `Beni Keller <https://codeberg.org/hb9hnt>`_

Ergänzendes Material und Korrekturen wurde von den folgenden Personen
beigesteuert:

* `Oliver Riesen <http://oriesen.ch>`_
* `Christoph Jüngling <https://github.com/juengling>`_

Fehler und Erweiterungen
========================

Der `Source-Code <https://codeberg.org/pythonbuch.com/pythonbuch-source>`_ dieses
Skripts wird aktuell auf `Codeberg <https://codeberg.org>`_ gehostet und kann von dort
mit Git geklont werden:

.. code-block:: bash

   $ git clone https://codeberg.org/pythonbuch.com/pythonbuch-source.git

Falls jemand Fehler im Skript korrigieren möchte oder neue Kapitel und
Erweiterungen in das Skript integrieren, sind die Korrekturen natürlich
willkommen. Dazu kann auf Codeberg ein 
`Issue <https://codeberg.org/pythonbuch.com/pythonbuch-source/issues>`_ 
erfasst oder noch besser ein 
`Pull Request <https://codeberg.org/pythonbuch.com/pythonbuch-source/pulls>`
mit den Korrekturen eingereicht werden.
